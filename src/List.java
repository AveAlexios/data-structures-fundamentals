public class List {
    private int[] array;
    private int counter;
    public List() {
        counter = 0;
    }
    public void add(int item) {
        int[] arraySecond;
        arraySecond = array;
        array = new int[counter + 1];

        for (int i = 0; i < counter; i++) {
            array[i] = arraySecond[i];
        }
        array[counter] = item;
        counter++;

    }
    public void insert(int item, int index) {
        if (index == counter) {
            add(item);
            return;
        }
        int[] arraySecond;
        arraySecond = array;
        array = new int[counter + 1];

        for (int i = 0; i < index; i++) {
            array[i] = arraySecond[i];
        }
        for (int i = index + 1; i < counter + 1; i++) {
            array[i] = arraySecond[i-1];
        }
        array[index] = item;
        counter++;
    }
    public int get(int index) {
        return array[index];
    }
    public int size() {
        return counter;
    }
}

